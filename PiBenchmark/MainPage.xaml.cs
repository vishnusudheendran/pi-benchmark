﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Diagnostics;

namespace PiBenchmark
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void btnbench_Click(object sender, RoutedEventArgs e)
        {
            btnbench.Visibility = Visibility.Collapsed;
            textBlock_Testing.Visibility = Visibility.Visible;
            textBlockResult.Text = string.Empty;
            Stopwatch timer = new Stopwatch();
            timer.Start();
            long i;
            Double pi = 0;
            double den = -1;
            for (i = 0; i <= 10000000; i++)
            {
                den += den + 2;
                pi += (4 * (-1) ^ i) / den;
            }
            timer.Stop();
            textBlock_Testing.Visibility = Visibility.Collapsed;
            textBlockResult.Visibility = Visibility.Visible;
            textBlockResult.Text = "Result: Elapsed time= " + timer.ElapsedMilliseconds + "Milliseconds";
            btnbench.Visibility = Visibility.Visible;
        }
    }
}